﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace derstekihesaptasarimi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (ekranLabel.Text == "0") ekranLabel.Text = "";
            ekranLabel.Text += "3";
            sayi6Button.BackColor = Color.White;
            sayi6Button.ForeColor = Color.Red;
            olayGunlugu.Items.Add("Button 4: Click");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ekranLabel.Text == "0") ekranLabel.Text = "";
            ekranLabel.Text += "1";
            sayi6Button.BackColor = Color.White;
            sayi6Button.ForeColor = Color.Black;
            olayGunlugu.Items.Add("Button 1: Click");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (ekranLabel.Text == "0") ekranLabel.Text = "";
            ekranLabel.Text += "2";
            sayi6Button.BackColor = Color.White;
            sayi6Button.ForeColor = Color.Black;
            olayGunlugu.Items.Add("Button 2: Click");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            ekranLabel.Text = "0";
        }

        private void sayi4Button_Click(object sender, EventArgs e)
        {
            if (ekranLabel.Text == "0") ekranLabel.Text = "";
            ekranLabel.Text += "4";
            olayGunlugu.Items.Add("Button 4: Click");
        }

        private void sayi5Button_Click(object sender, EventArgs e)
        {
            if (ekranLabel.Text == "0") ekranLabel.Text = "";
            ekranLabel.Text += "5";
            olayGunlugu.Items.Add("Button 5: Click");
        }

        private void sayi6Button_Click(object sender, EventArgs e)
        {
            if (ekranLabel.Text == "0") ekranLabel.Text = "";
            ekranLabel.Text += "6";
            olayGunlugu.Items.Add("Button 6: Click");
        }

        private void sayi7Button_Click(object sender, EventArgs e)
        {
            if (ekranLabel.Text == "0") ekranLabel.Text = "";
            ekranLabel.Text += "7";
            olayGunlugu.Items.Add("Button 7: Click");
        }

        private void sayi8Button_Click(object sender, EventArgs e)
        {
            if (ekranLabel.Text == "0") ekranLabel.Text = "";
            ekranLabel.Text += "8";
            olayGunlugu.Items.Add("Button 8: Click");
        }

        private void sayi9Button_Click(object sender, EventArgs e)
        {
            if (ekranLabel.Text == "0") ekranLabel.Text = "";
            ekranLabel.Text += "9";
            olayGunlugu.Items.Add("Button 9: Click");
        }

        private void sayi2Button_MouseEnter(object sender, EventArgs e)
        {
            sayi2Button.BackColor = Color.Chocolate;
            sayi2Button.ForeColor = Color.Black;
            olayGunlugu.Items.Add("Button 2: MouseEnter");
        }

        private void sayi6Button_MouseLeave(object sender, EventArgs e)
        {
            sayi6Button.BackColor = Color.White;
            sayi6Button.ForeColor = Color.Black;
            olayGunlugu.Items.Add("Button 6: MouseLeave");

        }


        private void sayi6Button_MouseEnter(object sender, EventArgs e)
        {
            sayi6Button.BackColor = Color.Chocolate;
            sayi6Button.ForeColor = Color.Black;

        }

        private void sayi2Button_MouseLeave(object sender, EventArgs e)
        {
            sayi2Button.BackColor = Color.White;
            sayi2Button.ForeColor = Color.Black;
        }

        private void sayi1Button_MouseEnter(object sender, EventArgs e)
        {
            sayi1Button.BackColor = Color.Chocolate;
            sayi1Button.ForeColor = Color.Black;
            olayGunlugu.Items.Add("Button 1: MouseEnter");
        }

        private void sayi1Button_MouseLeave(object sender, EventArgs e)
        {
            sayi1Button.BackColor = Color.White;
            sayi1Button.ForeColor = Color.Black;
        }

        private void sayi3Button_MouseEnter(object sender, EventArgs e)
        {
            sayi3Button.BackColor = Color.Chocolate;
            sayi3Button.ForeColor = Color.Black;
            olayGunlugu.Items.Add("Button 3: MouseEnter");
        }

        private void sayi3Button_MouseLeave(object sender, EventArgs e)
        {
            sayi3Button.BackColor = Color.White;
            sayi3Button.ForeColor = Color.Black;
        }

        private void sayi4Button_MouseEnter(object sender, EventArgs e)
        {
            sayi4Button.BackColor = Color.Chocolate;
            sayi4Button.ForeColor = Color.Black;
        }

        private void sayi4Button_MouseLeave(object sender, EventArgs e)
        {
            sayi4Button.BackColor = Color.White;
            sayi4Button.ForeColor = Color.Black;
        }

        private void sayi5Button_MouseEnter(object sender, EventArgs e)
        {
            sayi5Button.BackColor = Color.Chocolate;
            sayi5Button.ForeColor = Color.Black;
        }

        private void sayi5Button_MouseLeave(object sender, EventArgs e)
        {
            sayi5Button.BackColor = Color.White;
            sayi5Button.ForeColor = Color.Black;
        }

        private void sayi7Button_MouseEnter(object sender, EventArgs e)
        {
            sayi7Button.BackColor = Color.Chocolate;
            sayi7Button.ForeColor = Color.Black;
        }

        private void sayi7Button_MouseLeave(object sender, EventArgs e)
        {
            sayi7Button.BackColor = Color.White;
            sayi7Button.ForeColor = Color.Black;
        }

        private void sayi8Button_MouseEnter(object sender, EventArgs e)
        {
            sayi8Button.BackColor = Color.Chocolate;
            sayi8Button.ForeColor = Color.Black;
        }

        private void sayi8Button_MouseLeave(object sender, EventArgs e)
        {
            sayi8Button.BackColor = Color.White;
            sayi8Button.ForeColor = Color.Black;
        }

        private void sayi9Button_MouseEnter(object sender, EventArgs e)
        {
            sayi9Button.BackColor = Color.Chocolate;
            sayi9Button.ForeColor = Color.Black;
        }

        private void sayi9Button_MouseLeave(object sender, EventArgs e)
        {
            sayi9Button.BackColor = Color.White;
            sayi9Button.ForeColor = Color.Black;
        }

        private void sayi0Button_MouseEnter(object sender, EventArgs e)
        {
            sayi0Button.BackColor = Color.Chocolate;
            sayi0Button.ForeColor = Color.Black;
        }

        private void sayi0Button_MouseLeave(object sender, EventArgs e)
        {
            sayi0Button.BackColor = Color.White;
            sayi0Button.ForeColor = Color.Black;
            olayGunlugu.Items.Add("Button 0: MouseLeave");
        }

        private void sayi1Button_BackColorChanged(object sender, EventArgs e)
        {
            olayGunlugu.Items.Add("Button 1: BackColorChange");
        }
    }
}
