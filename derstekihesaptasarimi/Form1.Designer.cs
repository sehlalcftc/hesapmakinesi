﻿namespace derstekihesaptasarimi
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ekranLabel = new System.Windows.Forms.Label();
            this.sayi1Button = new System.Windows.Forms.Button();
            this.sayi2Button = new System.Windows.Forms.Button();
            this.artiButton = new System.Windows.Forms.Button();
            this.sayi3Button = new System.Windows.Forms.Button();
            this.eksiButton = new System.Windows.Forms.Button();
            this.sayi6Button = new System.Windows.Forms.Button();
            this.sayi5Button = new System.Windows.Forms.Button();
            this.sayi4Button = new System.Windows.Forms.Button();
            this.boluButton = new System.Windows.Forms.Button();
            this.esittirButton = new System.Windows.Forms.Button();
            this.sayi0Button = new System.Windows.Forms.Button();
            this.cButton = new System.Windows.Forms.Button();
            this.carpiButton = new System.Windows.Forms.Button();
            this.sayi9Button = new System.Windows.Forms.Button();
            this.sayi8Button = new System.Windows.Forms.Button();
            this.sayi7Button = new System.Windows.Forms.Button();
            this.olayGunlugu = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // ekranLabel
            // 
            this.ekranLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ekranLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ekranLabel.Font = new System.Drawing.Font("Arial Narrow", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ekranLabel.Location = new System.Drawing.Point(54, 73);
            this.ekranLabel.Name = "ekranLabel";
            this.ekranLabel.Size = new System.Drawing.Size(281, 73);
            this.ekranLabel.TabIndex = 0;
            this.ekranLabel.Text = "0";
            this.ekranLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // sayi1Button
            // 
            this.sayi1Button.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.sayi1Button.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.sayi1Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sayi1Button.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sayi1Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi1Button.Location = new System.Drawing.Point(54, 170);
            this.sayi1Button.Name = "sayi1Button";
            this.sayi1Button.Size = new System.Drawing.Size(60, 60);
            this.sayi1Button.TabIndex = 1;
            this.sayi1Button.Text = "1";
            this.sayi1Button.UseVisualStyleBackColor = false;
            this.sayi1Button.BackColorChanged += new System.EventHandler(this.sayi1Button_BackColorChanged);
            this.sayi1Button.Click += new System.EventHandler(this.button1_Click);
            this.sayi1Button.MouseEnter += new System.EventHandler(this.sayi1Button_MouseEnter);
            this.sayi1Button.MouseLeave += new System.EventHandler(this.sayi1Button_MouseLeave);
            // 
            // sayi2Button
            // 
            this.sayi2Button.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.sayi2Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sayi2Button.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sayi2Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi2Button.Location = new System.Drawing.Point(129, 170);
            this.sayi2Button.Name = "sayi2Button";
            this.sayi2Button.Size = new System.Drawing.Size(60, 60);
            this.sayi2Button.TabIndex = 2;
            this.sayi2Button.Text = "2";
            this.sayi2Button.UseVisualStyleBackColor = false;
            this.sayi2Button.Click += new System.EventHandler(this.button2_Click);
            this.sayi2Button.MouseEnter += new System.EventHandler(this.sayi2Button_MouseEnter);
            this.sayi2Button.MouseLeave += new System.EventHandler(this.sayi2Button_MouseLeave);
            // 
            // artiButton
            // 
            this.artiButton.BackColor = System.Drawing.Color.PeachPuff;
            this.artiButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.artiButton.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.artiButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.artiButton.Location = new System.Drawing.Point(275, 170);
            this.artiButton.Name = "artiButton";
            this.artiButton.Size = new System.Drawing.Size(60, 60);
            this.artiButton.TabIndex = 4;
            this.artiButton.Text = "+";
            this.artiButton.UseVisualStyleBackColor = false;
            // 
            // sayi3Button
            // 
            this.sayi3Button.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.sayi3Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sayi3Button.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sayi3Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi3Button.Location = new System.Drawing.Point(200, 170);
            this.sayi3Button.Name = "sayi3Button";
            this.sayi3Button.Size = new System.Drawing.Size(60, 60);
            this.sayi3Button.TabIndex = 3;
            this.sayi3Button.Text = "3";
            this.sayi3Button.UseVisualStyleBackColor = false;
            this.sayi3Button.Click += new System.EventHandler(this.button4_Click);
            this.sayi3Button.MouseEnter += new System.EventHandler(this.sayi3Button_MouseEnter);
            this.sayi3Button.MouseLeave += new System.EventHandler(this.sayi3Button_MouseLeave);
            // 
            // eksiButton
            // 
            this.eksiButton.BackColor = System.Drawing.Color.PeachPuff;
            this.eksiButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.eksiButton.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.eksiButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.eksiButton.Location = new System.Drawing.Point(275, 236);
            this.eksiButton.Name = "eksiButton";
            this.eksiButton.Size = new System.Drawing.Size(60, 60);
            this.eksiButton.TabIndex = 8;
            this.eksiButton.Text = "-";
            this.eksiButton.UseVisualStyleBackColor = false;
            // 
            // sayi6Button
            // 
            this.sayi6Button.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.sayi6Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sayi6Button.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sayi6Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi6Button.Location = new System.Drawing.Point(200, 236);
            this.sayi6Button.Name = "sayi6Button";
            this.sayi6Button.Size = new System.Drawing.Size(60, 60);
            this.sayi6Button.TabIndex = 7;
            this.sayi6Button.Text = "6";
            this.sayi6Button.UseVisualStyleBackColor = false;
            this.sayi6Button.Click += new System.EventHandler(this.sayi6Button_Click);
            this.sayi6Button.MouseEnter += new System.EventHandler(this.sayi6Button_MouseEnter);
            this.sayi6Button.MouseLeave += new System.EventHandler(this.sayi6Button_MouseLeave);
            // 
            // sayi5Button
            // 
            this.sayi5Button.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.sayi5Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sayi5Button.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sayi5Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi5Button.Location = new System.Drawing.Point(129, 236);
            this.sayi5Button.Name = "sayi5Button";
            this.sayi5Button.Size = new System.Drawing.Size(60, 60);
            this.sayi5Button.TabIndex = 6;
            this.sayi5Button.Text = "5";
            this.sayi5Button.UseVisualStyleBackColor = false;
            this.sayi5Button.Click += new System.EventHandler(this.sayi5Button_Click);
            this.sayi5Button.MouseEnter += new System.EventHandler(this.sayi5Button_MouseEnter);
            this.sayi5Button.MouseLeave += new System.EventHandler(this.sayi5Button_MouseLeave);
            // 
            // sayi4Button
            // 
            this.sayi4Button.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.sayi4Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sayi4Button.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sayi4Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi4Button.Location = new System.Drawing.Point(54, 236);
            this.sayi4Button.Name = "sayi4Button";
            this.sayi4Button.Size = new System.Drawing.Size(60, 60);
            this.sayi4Button.TabIndex = 5;
            this.sayi4Button.Text = "4";
            this.sayi4Button.UseVisualStyleBackColor = false;
            this.sayi4Button.Click += new System.EventHandler(this.sayi4Button_Click);
            this.sayi4Button.MouseEnter += new System.EventHandler(this.sayi4Button_MouseEnter);
            this.sayi4Button.MouseLeave += new System.EventHandler(this.sayi4Button_MouseLeave);
            // 
            // boluButton
            // 
            this.boluButton.BackColor = System.Drawing.Color.PeachPuff;
            this.boluButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.boluButton.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.boluButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.boluButton.Location = new System.Drawing.Point(275, 369);
            this.boluButton.Name = "boluButton";
            this.boluButton.Size = new System.Drawing.Size(60, 60);
            this.boluButton.TabIndex = 16;
            this.boluButton.Text = "/";
            this.boluButton.UseVisualStyleBackColor = false;
            // 
            // esittirButton
            // 
            this.esittirButton.BackColor = System.Drawing.Color.Khaki;
            this.esittirButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.esittirButton.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.esittirButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.esittirButton.Location = new System.Drawing.Point(200, 369);
            this.esittirButton.Name = "esittirButton";
            this.esittirButton.Size = new System.Drawing.Size(60, 60);
            this.esittirButton.TabIndex = 15;
            this.esittirButton.Text = "=";
            this.esittirButton.UseVisualStyleBackColor = false;
            // 
            // sayi0Button
            // 
            this.sayi0Button.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.sayi0Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sayi0Button.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sayi0Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi0Button.Location = new System.Drawing.Point(129, 369);
            this.sayi0Button.Name = "sayi0Button";
            this.sayi0Button.Size = new System.Drawing.Size(60, 60);
            this.sayi0Button.TabIndex = 14;
            this.sayi0Button.Text = "0";
            this.sayi0Button.UseVisualStyleBackColor = false;
            this.sayi0Button.MouseEnter += new System.EventHandler(this.sayi0Button_MouseEnter);
            this.sayi0Button.MouseLeave += new System.EventHandler(this.sayi0Button_MouseLeave);
            // 
            // cButton
            // 
            this.cButton.BackColor = System.Drawing.Color.MediumTurquoise;
            this.cButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cButton.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.cButton.Location = new System.Drawing.Point(54, 369);
            this.cButton.Name = "cButton";
            this.cButton.Size = new System.Drawing.Size(60, 60);
            this.cButton.TabIndex = 13;
            this.cButton.Text = "C";
            this.cButton.UseVisualStyleBackColor = false;
            this.cButton.Click += new System.EventHandler(this.button12_Click);
            // 
            // carpiButton
            // 
            this.carpiButton.BackColor = System.Drawing.Color.PeachPuff;
            this.carpiButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.carpiButton.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.carpiButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.carpiButton.Location = new System.Drawing.Point(275, 303);
            this.carpiButton.Name = "carpiButton";
            this.carpiButton.Size = new System.Drawing.Size(60, 60);
            this.carpiButton.TabIndex = 12;
            this.carpiButton.Text = "*";
            this.carpiButton.UseVisualStyleBackColor = false;
            // 
            // sayi9Button
            // 
            this.sayi9Button.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.sayi9Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sayi9Button.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sayi9Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi9Button.Location = new System.Drawing.Point(200, 303);
            this.sayi9Button.Name = "sayi9Button";
            this.sayi9Button.Size = new System.Drawing.Size(60, 60);
            this.sayi9Button.TabIndex = 11;
            this.sayi9Button.Text = "9";
            this.sayi9Button.UseVisualStyleBackColor = false;
            this.sayi9Button.Click += new System.EventHandler(this.sayi9Button_Click);
            this.sayi9Button.MouseEnter += new System.EventHandler(this.sayi9Button_MouseEnter);
            this.sayi9Button.MouseLeave += new System.EventHandler(this.sayi9Button_MouseLeave);
            // 
            // sayi8Button
            // 
            this.sayi8Button.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.sayi8Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sayi8Button.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sayi8Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi8Button.Location = new System.Drawing.Point(129, 303);
            this.sayi8Button.Name = "sayi8Button";
            this.sayi8Button.Size = new System.Drawing.Size(60, 60);
            this.sayi8Button.TabIndex = 10;
            this.sayi8Button.Text = "8";
            this.sayi8Button.UseVisualStyleBackColor = false;
            this.sayi8Button.Click += new System.EventHandler(this.sayi8Button_Click);
            this.sayi8Button.MouseEnter += new System.EventHandler(this.sayi8Button_MouseEnter);
            this.sayi8Button.MouseLeave += new System.EventHandler(this.sayi8Button_MouseLeave);
            // 
            // sayi7Button
            // 
            this.sayi7Button.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.sayi7Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sayi7Button.Font = new System.Drawing.Font("Arial Narrow", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sayi7Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.sayi7Button.Location = new System.Drawing.Point(54, 303);
            this.sayi7Button.Name = "sayi7Button";
            this.sayi7Button.Size = new System.Drawing.Size(60, 60);
            this.sayi7Button.TabIndex = 9;
            this.sayi7Button.Text = "7";
            this.sayi7Button.UseVisualStyleBackColor = false;
            this.sayi7Button.Click += new System.EventHandler(this.sayi7Button_Click);
            this.sayi7Button.MouseEnter += new System.EventHandler(this.sayi7Button_MouseEnter);
            this.sayi7Button.MouseLeave += new System.EventHandler(this.sayi7Button_MouseLeave);
            // 
            // olayGunlugu
            // 
            this.olayGunlugu.FormattingEnabled = true;
            this.olayGunlugu.ItemHeight = 16;
            this.olayGunlugu.Location = new System.Drawing.Point(350, 170);
            this.olayGunlugu.Name = "olayGunlugu";
            this.olayGunlugu.Size = new System.Drawing.Size(170, 260);
            this.olayGunlugu.TabIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(532, 478);
            this.Controls.Add(this.olayGunlugu);
            this.Controls.Add(this.boluButton);
            this.Controls.Add(this.esittirButton);
            this.Controls.Add(this.sayi0Button);
            this.Controls.Add(this.cButton);
            this.Controls.Add(this.carpiButton);
            this.Controls.Add(this.sayi9Button);
            this.Controls.Add(this.sayi8Button);
            this.Controls.Add(this.sayi7Button);
            this.Controls.Add(this.eksiButton);
            this.Controls.Add(this.sayi6Button);
            this.Controls.Add(this.sayi5Button);
            this.Controls.Add(this.sayi4Button);
            this.Controls.Add(this.artiButton);
            this.Controls.Add(this.sayi3Button);
            this.Controls.Add(this.sayi2Button);
            this.Controls.Add(this.sayi1Button);
            this.Controls.Add(this.ekranLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label ekranLabel;
        private System.Windows.Forms.Button sayi1Button;
        private System.Windows.Forms.Button sayi2Button;
        private System.Windows.Forms.Button artiButton;
        private System.Windows.Forms.Button sayi3Button;
        private System.Windows.Forms.Button eksiButton;
        private System.Windows.Forms.Button sayi6Button;
        private System.Windows.Forms.Button sayi5Button;
        private System.Windows.Forms.Button sayi4Button;
        private System.Windows.Forms.Button boluButton;
        private System.Windows.Forms.Button esittirButton;
        private System.Windows.Forms.Button sayi0Button;
        private System.Windows.Forms.Button cButton;
        private System.Windows.Forms.Button carpiButton;
        private System.Windows.Forms.Button sayi9Button;
        private System.Windows.Forms.Button sayi8Button;
        private System.Windows.Forms.Button sayi7Button;
        private System.Windows.Forms.ListBox olayGunlugu;
    }
}

